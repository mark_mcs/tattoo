module service;

import core.sys.windows.windows;
import core.sys.windows.wingdi;
import core.sys.windows.com;
import std.stdio;
import std.typecons : scoped;

import bgpool;
import config : Config;
import filoader;
import helpers;
import types;
import ui.window;
import win32.windef_dpi;

void runBackgroundService(ref Config config)
{
    auto desktopHandle = findCurrentDesktopWindow();
	if (desktopHandle == INVALID_HANDLE_VALUE) {
		throw new Exception("Cant find worker window");
	}

	auto imagePool = scoped!BgPool();
	auto parentWindow = scoped!Window(desktopHandle);
	auto monitors = buildMonitorList(parentWindow, config, imagePool);

    // this is here so we can receive WM_DISPLAYCHANGE
	auto messageWindow = scoped!BroadcastWindow((HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
		if (msg == WM_DISPLAYCHANGE || msg == WM_DPICHANGED)
		{
			writeln ("Got ", (msg == WM_DISPLAYCHANGE ? "WM_DISPLAYCHANGE" : "WM_DPICHANGED"), ". Rebuilding windows.");
			destroyMonitorList(monitors);
			monitors = buildMonitorList(parentWindow, config, imagePool);
			return 0;
		}
		return DefWindowProc(hwnd, msg, wParam, lParam);
	});


	MSG msg;
	BOOL ret;
	while ((ret = GetMessage(&msg, null, 0, 0)) != 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}


private Monitor[] buildMonitorList(Window parentWindow, ref Config config, BgPool imagePool)
{
	auto monitors = findMonitors();

	writefln("found %d monitors:", monitors.length);
	foreach (m; monitors) 
	{
		auto bounds = m.bounds;
		writefln("name [%s], dpi [%d], bounds [%d,%d,%d,%d]", m.name, m.dpi,
			bounds.left, bounds.top, bounds.right, bounds.bottom);

		string imagePath = m.findImageForMonitor(config);
		if (imagePath == "") 
		{
			writeln("No image for monitor");
			continue;
		}

		imagePool.getOrLoadImage(imagePath);

		m.managedWindow = new Window(""w, parentWindow, imagePool);
		m.managedWindow.backgroundName = imagePath;
		m.managedWindow.visible = true;
	}

	return monitors;
}


private void destroyMonitorList(Monitor[] monitors)
{
	foreach (m; monitors)
	{
		destroy(m);
	}
}
