module bgpool;

import core.lifetime : move;

import filoader;
import types : GdiImage;


class BgPool
{
    private GdiImage[] _images;


    void addImage(string path)
    {
        auto loaded = loadImageFile(path);
        if (loaded != GdiImage.init)
        {
            _images.length++;
            loaded.move(_images[$-1]);
        }
    }

    GdiImage* getImage(string name)
    {
        foreach (ref img; _images) 
        {
            if (img.name == name)
                return &img;
        }
        return null;
    }

    GdiImage* getOrLoadImage(string path)
    {
        auto img = getImage(path);
        if (!img) {
            addImage(path);
            img = getImage(path);
        }
        return img;
    }
}