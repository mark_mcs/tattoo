module win32.windef_dpi;

extern(Windows):
nothrow:

private import core.sys.windows.basetsd;
import core.sys.windows.windef;

enum WM_DPICHANGED                   = 0x02E0;
enum WM_DPICHANGED_BEFOREPARENT      = 0x02E2;
enum WM_DPICHANGED_AFTERPARENT       = 0x02E3;

alias DPI_AWARENESS_CONTEXT = HANDLE;
enum DPI_AWARENESS_CONTEXT_UNAWARE              = (cast(DPI_AWARENESS_CONTEXT)-1);
enum DPI_AWARENESS_CONTEXT_SYSTEM_AWARE         = (cast(DPI_AWARENESS_CONTEXT)-2);
enum DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE    = (cast(DPI_AWARENESS_CONTEXT)-3);
enum DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2 = (cast(DPI_AWARENESS_CONTEXT)-4);
enum DPI_AWARENESS_CONTEXT_UNAWARE_GDISCALED    = (cast(DPI_AWARENESS_CONTEXT)-5);

DPI_AWARENESS_CONTEXT SetThreadDpiAwarenessContext(
  DPI_AWARENESS_CONTEXT dpiContext
);


alias MONITOR_DPI_TYPE = int;
enum : MONITOR_DPI_TYPE {
    MDT_EFFECTIVE_DPI = 0,
    MDT_ANGULAR_DPI = 1,
    MDT_RAW_DPI = 2,
    MDT_DEFAULT = MDT_EFFECTIVE_DPI
}

HRESULT GetDpiForMonitor(
  HMONITOR         hmonitor,
  MONITOR_DPI_TYPE dpiType,
  UINT             *dpiX,
  UINT             *dpiY
);