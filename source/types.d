import core.sys.windows.windef;
import core.sys.windows.winuser;
import core.sys.windows.wingdi;

import std.stdio : writefln;
import std.typecons : Tuple;

import ui.window;


struct Point
{
	int x;
	int y;
}


class Monitor 
{
	private {
		HMONITOR _handle;
		RECT _bounds;
		wstring _name;
		uint _dpi;
		Window _managedWindow;
	}

	this(HMONITOR handle, RECT bounds)
	{
		_handle = handle;
		_bounds = bounds;
	}

	~this() 
	{
		destroy(_managedWindow);
	}


	auto handle() { return _handle; }

	auto bounds() { return _bounds; }

	wstring name() { return _name; }
	void name(wstring value) { _name = value; }

	uint dpi() { return _dpi; }
	void dpi(uint value) { _dpi = value; }

	auto managedWindow() { return _managedWindow; }
	void managedWindow(Window w) 
	{
		_managedWindow = w;
		_managedWindow.bounds = _bounds;
		_managedWindow.update();
	}
}


struct GdiImage
{
	private HBITMAP _handle;

	immutable string name;

	this(string name, HBITMAP handle)
	{
		_handle = handle;
		this.name = name;
	}

	@disable this(this);

	~this()
	{
		if(_handle)
		{
			writefln("Destroying background image.");
			DeleteObject(_handle);
		}
	}

	HBITMAP handle() { return _handle; }
}
