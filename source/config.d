module config;

import std.file : exists, readText;
import std.stdio;
import std.json;

import asdf;

import types : Point;


struct Config
{
	struct MonitorEntry
	{
		struct Match {
			Point pos;
			Point size;
		}

		Match match;
		string backgroundImagePath;
	}

    string defaultBackground;
	MonitorEntry[] monitors;

	void addMonitorEntry(MonitorEntry e) 
	{
		monitors ~= e;
	}
}


Config loadConfig()
{
    enum CONFIG_PATH = "config.json";
    if (!CONFIG_PATH.exists()) return Config.init;

    return CONFIG_PATH.readText().deserialize!Config();
}

