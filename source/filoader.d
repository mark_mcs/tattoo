module filoader;

import core.stdc.stdio;
import core.sys.windows.wingdi;
import core.sys.windows.windows;
import std.stdio;
import std.string;
import freeimage;

import types : GdiImage;

GdiImage loadImageFile(string path)
{
    FreeImage_SetOutputMessage(&fi_outputMessage);

    auto pathz = path.toStringz();
    auto type = FreeImage_GetFileType(pathz);
    auto imageData = FreeImage_Load(type, pathz);

    auto screen = GetDC(null);
    auto bitmap = CreateDIBitmap(screen,
            FreeImage_GetInfoHeader(imageData),
            CBM_INIT, 
			FreeImage_GetBits(imageData),
			FreeImage_GetInfo(imageData),
			DIB_RGB_COLORS);
            
    ReleaseDC(null, screen);
    FreeImage_Unload(imageData);

    return GdiImage(path, bitmap);
}

extern(C)
{
    void fi_outputMessage(FREE_IMAGE_FORMAT fif, const(char*) msg)
    {
        printf("Error: %s\n\n", msg);
    }

    uint fi_fileRead(void* buffer, uint size, uint count, fi_handle handle) nothrow
    {
        try
        {
            File* f = cast(File*) handle;
            auto byteBuffer = (cast(byte*) buffer)[0..size*count];
            auto result = f.rawRead(byteBuffer);
            return cast(uint) (result.length / size);
        }
        catch (Exception e)
        {
            printf("File read error\n");
        }
        return 0;
    }

    uint fi_fileWrite(void* buffer, uint size, uint count, fi_handle handle) nothrow
    {
        try
        {
            auto f = cast(File*) handle;
            auto byteBuffer = (cast(byte*) buffer)[0..size*count];
            f.rawWrite(byteBuffer);
            return count;
        }
        catch (Exception e)
        {
            printf("File write error\n");
        }
        return 0;
    }


    int fi_seek(fi_handle handle, long offset, int origin) nothrow
    {
        try 
        {
            auto f = cast(File*) handle;
            f.seek(offset, origin);
            return 0;
        }
        catch (Exception e)
        {
            printf("seek error\n");
        }

        return -1;
    }

    long fi_tell(fi_handle handle) nothrow
    {
        try
        {
            return (cast(File*) handle).tell();
        }
        catch (Exception e) 
        {
            printf("tell error\n");
        }
        return -1;
    }

}


