module ui.window;

import std.stdio;
import std.string;

import core.lifetime : move;
import core.stdc.stdio : printf;

import core.sys.windows.wingdi;
import core.sys.windows.windows;
import core.sys.windows.winuser;

import types;
import bgpool;


void initUI(HINSTANCE hinst)
{
    WNDCLASSEX wc;
	with (wc) {
		style = CS_HREDRAW|CS_VREDRAW;
		lpfnWndProc = &internalWindowProc;
		hInstance = hinst;
		lpszClassName = THE_WINDOW_CLASS.ptr;
		if (!RegisterClassEx(&wc)) 
			throw new Exception("Failed to register window class");
    }

    wc = WNDCLASSEX.init;
    with (wc) {
		lpfnWndProc = &internalWindowProc;
		hInstance = hinst;
		lpszClassName = BROADCAST_WINDOW_CLASS.ptr;
		if (!RegisterClassEx(&wc)) 
			throw new Exception("Failed to register window class");
    }
}

private
{
    enum THE_WINDOW_CLASS = "TattooWnd_Class"w;
    enum BROADCAST_WINDOW_CLASS = "TattooBcastWnd_Class"w;
    enum PROP_INSTANCE_KEY = "mcs_WindowInstance"w;

    interface WndProcSupport
    {
        LRESULT processMessage(UINT msg, WPARAM wParam, LPARAM lParam);
    }


    extern (Windows) 
    LRESULT internalWindowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) nothrow
    {
        if (msg == WM_NCCREATE)
        {
            CREATESTRUCT* cs = cast(CREATESTRUCT*) lParam;
            auto self = cs.lpCreateParams;
            if (self) 
            {
                SetWindowLongPtr(hwnd, GWLP_USERDATA, cast(LONG_PTR) self);
            }
            return DefWindowProc(hwnd, msg, wParam, lParam);
        }

        auto windowInstance = cast(void*) GetWindowLongPtr(hwnd, GWLP_USERDATA);
        if (windowInstance) 
        {
            auto proc = cast(Window) windowInstance;
            try
            {
                return proc.processMessage(msg, wParam, lParam);
            }
            catch (Exception e)
            {
                return -1;
            }
        } else {
            return DefWindowProc(hwnd, msg, wParam, lParam);
        }
    }
}


enum ZOrder 
{
    Top,
    Bottom
}


class Window : WndProcSupport
{
	private {
		HWND _handle;
		Window _parent;
        BgPool _backgroundPool;
        string _backgroundName;
	}


	this(wstring title = ""w, Window parent = null, BgPool backgroundPool = null)
	{
		auto myhwnd = CreateWindowEx(0, 
                THE_WINDOW_CLASS.ptr,
                ""w.ptr,
                WS_CHILD, 
				0, 0, 10, 10, 
				parent ? parent.handle() : null,
				null, 
				GetModuleHandle(null), 
				cast(LPVOID) this);
		if (!myhwnd) {
			throw new Exception("Failed to create window");
		}

		_parent = parent;
        _backgroundPool = backgroundPool;
		this(myhwnd);
	}


    this(HWND existingHandle) 
    {
        _handle = existingHandle;
    }


    ~this()
    {
        if (_handle) 
        {
            writefln("Destroying window handle %8x", _handle);
            DestroyWindow(_handle);
        }
        _handle = null;
    }

    void bounds(RECT rect)
    {
        SetWindowPos(_handle, 
                cast(void*)0,
                rect.left,
                rect.top,
                rect.right - rect.left,
                rect.bottom - rect.top,
                SWP_NOZORDER);
    }

    RECT bounds()
    {
        RECT rc;
        GetWindowRect(_handle, &rc);
        return rc;
    }


    void visible(bool value)
    {
        ShowWindow(_handle, value ? SW_SHOW : SW_HIDE);
    }


    void backgroundName(string name) { _backgroundName = name; }


    void update()
    {
        UpdateWindow(_handle);
    }

    override LRESULT processMessage(UINT msg, WPARAM wParam, LPARAM lParam)
    {
        switch (msg)
        {
            case WM_PAINT:
                if(!_backgroundPool)
                {
                    goto default;
                }
                auto background = _backgroundPool.getOrLoadImage(_backgroundName);
                if (!background)
                {
                    writefln("Could not load image at path %s", _backgroundName);
                    return DefWindowProc(_handle, msg, wParam, lParam);
                }

                PAINTSTRUCT ps;
                BeginPaint(_handle, &ps);
                auto winDC = GetDC(_handle);

                RECT rc;
                GetClientRect(_handle, &rc);

                // HBRUSH brush = CreateSolidBrush(RGB(50, 151, 151));
                // FillRect(winDC, &rc, brush);
                // DeleteObject(brush);

                auto bmpDC = CreateCompatibleDC(winDC);
                SelectObject(bmpDC, background.handle());
                BITMAP bmp;
                GetObject(background.handle(), BITMAP.sizeof, &bmp);
                StretchBlt(winDC, rc.left, rc.top,
                    rc.right - rc.left,
                    rc.bottom - rc.top,
                    bmpDC,
                    0, 0,
                    bmp.bmWidth,
                    bmp.bmHeight,
                    SRCCOPY);

                DeleteDC(bmpDC);
                ReleaseDC(_handle, winDC);
                EndPaint(_handle, &ps);
                return 0;
            
            default:
                return DefWindowProc(_handle, msg, wParam, lParam);
        }
    }


	auto handle() { return _handle; }
	auto parent() { return _parent; }
}


alias MessageCallback = LRESULT delegate(HWND hwnd, UINT msg, WPARAM, LPARAM);

class BroadcastWindow : Window
{
    private MessageCallback _cbk;

    this(MessageCallback cbk)
    {
        _cbk = cbk;
        auto myhwnd = CreateWindowEx(0, 
                BROADCAST_WINDOW_CLASS.ptr,
                null,
                WS_OVERLAPPEDWINDOW, 
				0, 0, 1, 1, 
				HWND_DESKTOP,
				null, 
				GetModuleHandle(null), 
                cast(LPVOID) this);
        super(myhwnd);
    }

    override LRESULT processMessage(UINT msg, WPARAM wParam, LPARAM lParam)
    {
        return _cbk(handle(), msg, wParam, lParam);
    }
}