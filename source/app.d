import std.stdio;

import core.sys.windows.windows;
import core.lifetime : move;
import win32.windef_dpi;

import config;
import helpers;
import service;
import types;
import ui.window;



void main(string[] args)
{
	bool serviceMode = args.length > 1 && args[1] == "--service";

	auto config = loadConfig();
	if (config == Config.init && serviceMode)
	{
		writefln("Configuration [config.json] not found. Please create a file and restart the program.");
		return;
	}

	initUI(GetModuleHandle(null));

	if (!SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2)) 
	{
		SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE);
	}

	//if (serviceMode)
	//{
		runBackgroundService(config);
	//}
	//else
	//{
	//	import filoader;
	//	auto bmp = loadImageFile("c:/Users/BarnettM/Pictures/20190804_055347.jpg");
	//}
}
