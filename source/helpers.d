module helpers;

import std.stdio;

import core.stdc.wchar_ : wcslen;
import core.sys.windows.windows;
import win32.windef_dpi;

import config : Config;
import types;


Monitor[] findMonitors()
{
    Monitor[] monitors;
    EnumDisplayMonitors(null, null, &foundMonitor, cast(LPARAM) &monitors);
    return monitors;
}

extern(Windows)
private BOOL foundMonitor(HMONITOR hMonitor, HDC hDc, LPRECT rect, LPARAM lParam) nothrow
{
	MONITORINFOEX mi;
	if (GetMonitorInfo(hMonitor, &mi) == 0)
	{
		return FALSE;
	}

	uint dpiX, dpiY;
	GetDpiForMonitor(hMonitor, MDT_EFFECTIVE_DPI, &dpiX, &dpiY);

	try 
	{
        auto monitors = cast(Monitor[]*) lParam;
		auto m = new Monitor(hMonitor, mi.rcMonitor);
		m.name = mi.szDevice[0..wcslen(mi.szDevice.ptr)].dup;
		m.dpi = dpiX;
		*monitors ~= m;
	}
	catch (Exception e)
	{

	}

	return TRUE;
}


/**
 * Find the desktop background window and set up the necessary reparenting
 * to allow us to insert our background windows.
 */
HWND findCurrentDesktopWindow() 
{
	extern(Windows) BOOL findWorkerWindowProc(HWND hwnd, LPARAM lParam) nothrow
	{
		auto viewhwnd = FindWindowEx(hwnd, null, "SHELLDLL_DefView", null);
		if (viewhwnd)
		{
			*(cast(HWND*) lParam) = FindWindowEx(null, hwnd, "WorkerW", null);
			return FALSE;
		}
		return TRUE;
	}

	// spawn the WorkerW
	auto hwProgman = FindWindow("Progman"w.ptr, null);
	if (!hwProgman){
		throw new Exception("Couldn't find desktop");
	}
	DWORD smtresult;
	SendMessageTimeout(hwProgman, 0x052C, 0, 0, SMTO_NORMAL, 1000, &smtresult);

	HWND parent = INVALID_HANDLE_VALUE;
	EnumWindows(&findWorkerWindowProc, cast(LPARAM) &parent);

	return parent;
}


/**
 * For a given monitor find the MonitorEntry in the config file that tells us
 * which background image to use.
 */
string findImageForMonitor(Monitor monitor, ref Config config)
{
	foreach (ref cm; config.monitors)
	{
		if (cm.match.pos.x == monitor.bounds.left
			&& cm.match.pos.y == monitor.bounds.top)
        {
			writeln("identified config match for monitor [", monitor.bounds.left,
					",", monitor.bounds.top, "]");
			return cm.backgroundImagePath;
		}
	}

	return config.defaultBackground;
}