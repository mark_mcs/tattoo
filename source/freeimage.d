module freeimage;

import core.sys.windows.wingdi : BITMAPINFO, BITMAPINFOHEADER;

extern(C):


enum FREEIMAGE_MAJOR_VERSION = 3;
enum FREEIMAGE_MINOR_VERSION = 18;
enum FREEIMAGE_RELEASE_SERIAL = 0;

struct FIBITMAP {
    void* data;
}

struct FIMULTIBITMAP {
    void* data;
}

enum FALSE = 0;
enum TRUE = 1;

enum SEEK_SET = 0;
enum SEEK_CUR = 1;
enum SEEK_END = 2;


alias BOOL  = int;
alias BYTE  = ubyte;
alias WORD  = ushort;
alias DWORD = uint;
alias LONG  = int;
alias INT64 = long;
alias UINT64 = ulong;


struct RGBQUAD {
    align(1):
    BYTE rgbBlue;
    BYTE rgbGreen;
    BYTE rgbRed;
    BYTE rgbReserved;
}

struct RGBTRIPLE {
    align(1):
    BYTE rgbBlue;
    BYTE rgbGreen;
    BYTE rgbRed;
}


// struct BITMAPINFOHEADER {
//     DWORD   biSize;
//     LONG    biWidth;
//     LONG    biHeight;
//     WORD    biPlanes;
//     WORD    biBitCount;
//     DWORD   biCompression;
//     DWORD   biSizeImage;
//     LONG    biXPelsPerMeter;
//     LONG    biYPelsPerMeter;
//     DWORD   biClrUsed;
//     DWORD   biClrImportant;
// }

// struct BITMAPINFO {
//     BITMAPINFOHEADER    bmiHeader;
//     RGBQUAD[1]          bmiColors;
// }


struct FIRGB16 {
    align(1):
    WORD red;
    WORD green;
    WORD blue;
}

struct FIRGBA16 {
    align(1):
    WORD red;
    WORD green;
    WORD blue;
    WORD alpha;
}

struct FIRGBF {
    align(1):
    float red;
    float green;
    float blue;
}

struct FIRGBAF {
    align(1):
    float red;
    float green;
    float blue;
    float alpha;
}

struct FICOMPLEX {
    align(1):
    double r;
    double i;
}


enum FI_RGBA_RED			= 2;
enum FI_RGBA_GREEN			= 1;
enum FI_RGBA_BLUE			= 0;
enum FI_RGBA_ALPHA			= 3;
enum FI_RGBA_RED_MASK		= 0x00FF0000;
enum FI_RGBA_GREEN_MASK		= 0x0000FF00;
enum FI_RGBA_BLUE_MASK		= 0x000000FF;
enum FI_RGBA_ALPHA_MASK		= 0xFF000000;
enum FI_RGBA_RED_SHIFT		= 16;
enum FI_RGBA_GREEN_SHIFT	= 8;
enum FI_RGBA_BLUE_SHIFT		= 0;
enum FI_RGBA_ALPHA_SHIFT	= 24;

enum FI_RGBA_RGB_MASK = FI_RGBA_RED_MASK | FI_RGBA_GREEN_MASK | FI_RGBA_BLUE_MASK;

enum FI16_555_RED_MASK		= 0x7C00;
enum FI16_555_GREEN_MASK	= 0x03E0;
enum FI16_555_BLUE_MASK		= 0x001F;
enum FI16_555_RED_SHIFT		= 10;
enum FI16_555_GREEN_SHIFT 	= 5;
enum FI16_555_BLUE_SHIFT	= 0;
enum FI16_565_RED_MASK		= 0xF800;
enum FI16_565_GREEN_MASK	= 0x07E0;
enum FI16_565_BLUE_MASK		= 0x001F;
enum FI16_565_RED_SHIFT		= 11;
enum FI16_565_GREEN_SHIFT 	= 5;
enum FI16_565_BLUE_SHIFT	= 0;

// ICC profile support ------------------------------------------------------

enum FIICC_DEFAULT			= 0x00;
enum FIICC_COLOR_IS_CMYK	= 0x01;

struct FIICCPROFILE { 
	WORD    flags;	//! info flag
	DWORD	size;	//! profile's size measured in bytes
	void*   data;	//! points to a block of contiguous memory containing the profile
};

/** I/O image format identifiers.
*/
enum FREE_IMAGE_FORMAT {
	FIF_UNKNOWN = -1,
	FIF_BMP		= 0,
	FIF_ICO		= 1,
	FIF_JPEG	= 2,
	FIF_JNG		= 3,
	FIF_KOALA	= 4,
	FIF_LBM		= 5,
	FIF_IFF = FIF_LBM,
	FIF_MNG		= 6,
	FIF_PBM		= 7,
	FIF_PBMRAW	= 8,
	FIF_PCD		= 9,
	FIF_PCX		= 10,
	FIF_PGM		= 11,
	FIF_PGMRAW	= 12,
	FIF_PNG		= 13,
	FIF_PPM		= 14,
	FIF_PPMRAW	= 15,
	FIF_RAS		= 16,
	FIF_TARGA	= 17,
	FIF_TIFF	= 18,
	FIF_WBMP	= 19,
	FIF_PSD		= 20,
	FIF_CUT		= 21,
	FIF_XBM		= 22,
	FIF_XPM		= 23,
	FIF_DDS		= 24,
	FIF_GIF     = 25,
	FIF_HDR		= 26,
	FIF_FAXG3	= 27,
	FIF_SGI		= 28,
	FIF_EXR		= 29,
	FIF_J2K		= 30,
	FIF_JP2		= 31,
	FIF_PFM		= 32,
	FIF_PICT	= 33,
	FIF_RAW		= 34,
	FIF_WEBP	= 35,
	FIF_JXR		= 36
}


// IO Stuff -----------------------------------------------------------------
alias fi_handle = void*;
alias FI_ReadProc = uint function(void* buffer, uint size, uint count, fi_handle handle);
alias FI_WriteProc = uint function(void* buffer, uint size, uint count, fi_handle handle);
alias FI_SeekProc = int function(fi_handle, long offset, int origin);
alias FI_TellProc = long function(fi_handle);

struct FreeImageIO {
    align(1):
    FI_ReadProc read_proc;
    FI_WriteProc write_proc;
    FI_SeekProc seek_proc;
    FI_TellProc tell_proc;
}

struct FIMEMORY {
    void* data;
}

// Allocate / Clone / Unload routines ---------------------------------------
FIBITMAP* FreeImage_Allocate(int width, int height, int bpp, uint red_mask = 0, uint green_mask = 0, uint blue_mask = 0);
void FreeImage_Unload(FIBITMAP *dib);



FIBITMAP* FreeImage_Load(FREE_IMAGE_FORMAT fif, const(char*) filename, int flags = 0);

FREE_IMAGE_FORMAT FreeImage_GetFileType(const(char*) filename, int flags = 0);


BITMAPINFOHEADER* FreeImage_GetInfoHeader(FIBITMAP *dib);
BITMAPINFO* FreeImage_GetInfo(FIBITMAP *dib);


BYTE* FreeImage_GetBits(FIBITMAP *dib);

alias FreeImage_OutputMessageFunction = void function (FREE_IMAGE_FORMAT fif, const(char*) msg);
alias FreeImage_OutputMessageFunctionStdCall = void function (FREE_IMAGE_FORMAT fif, const(char*) msg); 

void FreeImage_SetOutputMessageStdCall(FreeImage_OutputMessageFunctionStdCall omf); 
void FreeImage_SetOutputMessage(FreeImage_OutputMessageFunction omf);